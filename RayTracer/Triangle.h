#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Shape.h"
#include "Vector3d.h"
class Triangle :public Shape{

public:
	Vector3d a, b, c;
	Color color;
	
	
	Triangle(){
		objectType = 3;
	}
	void print(){
		printf("Triangle\n");
		a.print();
		b.print();
		c.print();
		color.print();
		printf("%lf\n",refractiveIndex);
	}

	void draw(){
	
		glBegin(GL_TRIANGLES); {
			/*glColor3f(color.r, color.g, color.b);
			glVertex3f(a.x, a.y, a.z);
			glVertex3f(b.x, b.y, b.z);
			glVertex3f(c.x, c.y, c.z);
			*/
			glColor3f(color.r, color.g, color.b);
			glVertex3f(a.x, a.y, a.z);
			glVertex3f(b.x, b.y, b.z);
			glVertex3f(c.x, c.y, c.z);
		}glEnd();
	
	}
	void input(){
		char s[50];
		while (scanf("%s", s) == 1)
		{
			if (strcmp(s, "objEnd") == 0)break;
			else if (strcmp(s, "color") == 0)
			{
				color.input();
			}
			else if (strcmp(s, "ambCoeff") == 0){
				scanf("%lf", &ambientCoeff);
			}
			else if (strcmp(s, "difCoeff") == 0){
				scanf("%lf", &diffuseCoeff);
			}
			else if (strcmp(s, "refCoeff") == 0){
				scanf("%lf", &refCoeff);
			}
			else if (strcmp(s, "specCoeff") == 0){
				scanf("%lf", &specCoeff);
			}
			else if (strcmp(s, "specExp") == 0){
				scanf("%lf", &specExp);
			}
			else if (strcmp(s, "a") == 0){
				a.input();
			}
			else if (strcmp(s, "b") == 0){
				b.input();

			}
			else if (strcmp(s, "c") == 0){
				c.input();
			}
			else if (strcmp(s, "refractiveIndex") == 0){
				scanf("%lf", &refractiveIndex);
			}

		}
	}
	double rayIntersectsShape(Ray ray){
		Vector3d ab(a, b);
		Vector3d bc(b, c);
		Vector3d ac(a, c);
		Vector3d ca(c, a);
		Vector3d normal;
		normal = ab.cross(ac);
		normal = normal.normalize();
		double pt = (a.dot(normal) - ray.origin.dot(normal))/ray.direction.dot(normal);
		Vector3d p(ray.origin, pt, ray.direction);
		Vector3d ap(a, p);
		Vector3d bp(b, p);
		Vector3d cp(c, p);

		double aval=(ab.cross(ap)).dot(normal);
		double bval=(bc.cross(bp)).dot(normal);
		double cval=(ca.cross(cp)).dot(normal);

		if ((aval >= 0 && bval >= 0 && cval >= 0) || (aval <= 0 && bval <= 0 && cval <= 0))
		{
			return pt;
		}
		else return -1;
		

	}
	Color getColorAtPoint(Ray ray, Vector3d point, Lights l,vector<Shape *> allShapes){

		Vector3d normal1, normal2;
		Vector3d ab(a, b);
		Vector3d ac(a, c);
		normal1 = ab.cross(ac);
		normal2 = Vector3d(0, 0, 0) - normal1;

		Vector3d normal;
		if (normal1.dot(ray.direction) < 0)normal = normal1;
		else normal = normal2;

		normal=normal.normalize();
		int lighCount = l.allLights.size();
		Color lightedColor(0, 0, 0);

		for (int i = 0; i < lighCount; i++)
		{
			Vector3d incidentRay(point, *(l.allLights[i]));
			//Vector3d incidentRay(*(l.allLights[i]),point);

			//Vector3d normal(center, point);

			incidentRay = incidentRay.normalize();
		
			Ray inc(*(l.allLights[i]), incidentRay);
			double mint = 500000;
			Shape *intersectedObject = NULL;
			int shapeCount = allShapes.size();
			for (int j = 0; j<shapeCount; j++)
			{

				double t = (allShapes[j])->rayIntersectsShape(inc);

				if (t > 0 && t<mint)
				{
					intersectedObject = allShapes[j];
					mint = t;
					break;
				}
			}

			if (mint < 50000)continue;

			//Vector3d reflection = incidentRay - normal * 2 * (incidentRay.dot(normal));
			double lambert = incidentRay.dot(normal);

			Vector3d viewer(point, ray.origin);
			//Vector3d viewer(ray.origin, point);
			viewer = viewer.normalize();
			Vector3d halfVector = incidentRay + viewer;
			halfVector = halfVector.normalize();
			double phong = halfVector.dot(normal);


			Color diff = color*(diffuseCoeff*(lambert > 0 ? lambert : 0));
			double specPart = pow((phong > 0 ? phong : 0), specExp);
			Color spec = color*(specCoeff*specPart);

			//printf("%lf %lf\n", lambert, phong);

			lightedColor = lightedColor + (diff + spec)*(1.0 / lighCount);
		}

		lightedColor = lightedColor + color*ambientCoeff;

		return lightedColor;

	}
	
	Vector3d getNormal(Vector3d point, Ray ray){
		Vector3d normal1, normal2;
		Vector3d ab(a, b);
		Vector3d ac(a, c);
		normal1 = ab.cross(ac);
		normal2 = Vector3d(0, 0, 0) - normal1;

		Vector3d normal;
		if (normal1.dot(ray.direction) < 0)normal = normal1;
		else normal = normal2;

		normal = normal.normalize();

		return normal;
	}

};
#endif