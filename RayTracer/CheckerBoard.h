#ifndef CHECKERBOARD_H
#define CHECKERBOARD_H


#include "Ray.h"
#include "Shape.h"
class CheckerBoard: public Shape{

public:
	Color c1;
	Color c2;
	int range;
	CheckerBoard(){
		range = 500;
		refractiveIndex = -1;
		objectType = 1;
	}
	void print(){
		printf("CheckerBoard\n");
		printf("%d\n", objectType);
		c1.print();
		c2.print();
	}

	void input(){
		char s[50];
		while (scanf("%s",s)==1)
		{
			if (strcmp(s, "objEnd") == 0)break;
			else if (strcmp(s, "colorOne" )== 0)
			{
				c1.input();
			}
			else if (strcmp(s, "colorTwo" )== 0){
				c2.input();
			}
			else if (strcmp(s, "ambCoeff" )== 0){
				scanf("%lf", &ambientCoeff);
			}
			else if (strcmp(s, "difCoeff" )== 0){
				scanf("%lf", &diffuseCoeff);
			}
			else if (strcmp(s, "refCoeff" )== 0){
				scanf("%lf", &refCoeff);
			}
			else if (strcmp(s, "specCoeff" )== 0){
				scanf("%lf", &specCoeff);
			}
			else if (strcmp(s, "specExp" )== 0){
				scanf("%lf", &specExp);
			}
		}
	}

	void draw(){
	
		for (int i = -range; i < range; i += 10)
		{
			for (int j = -range; j < range; j += 10)
			{
				glBegin(GL_QUADS); {
					if ((i/10 + j/10) % 2 == 0){
						glColor3f(c1.r, c1.g, c1.b);
					}
					else glColor3f(c2.r, c2.g, c2.b);
					glVertex3f(i, 0,j);
					glVertex3f(i + 10, 0,j);
					glVertex3f(i + 10, 0,j + 10);
					glVertex3f(i, 0,j + 10);
				}glEnd();
			}
		}

	}

	double rayIntersectsShape(Ray ray)
	{
		Vector3d normal(0, 1, 0);
		Vector3d pNot(0, 0, 0);
		double pn = pNot.dot(normal);
		double on = ray.origin.dot(normal);
		double dn = ray.direction.dot(normal);
		double t = (pn - on) / (dn);
		return t;
	}
	
	Color getColorAtPoint(Ray ray, Vector3d point, Lights l, vector<Shape *> allShapes)
	{
		int x = ceil((point.x + range) / 10);
		int y = ceil((point.z + range) / 10);

		//if (x > 30 || y > 30) return Color(0, 0, 0);

		Vector3d normal(0, 1, 0);
		normal = normal.normalize();

		Color primaryColor;
		if (!pointOnCheckerBoard(point))return Color(0, 0, 0);

		else if ((x+y) % 2 == 0){
			primaryColor.setColor(c1);
		}
		else primaryColor.setColor(c2);

		int lighCount = l.allLights.size();
		Color color(0, 0, 0);

		for (int i = 0; i < lighCount; i++)
		{
			Vector3d incidentRay(point, *(l.allLights[i]));
			incidentRay = incidentRay.normalize();

			Ray inc(*(l.allLights[i]), incidentRay);
			double mint = 500000;
			Shape *intersectedObject = NULL;
			int shapeCount = allShapes.size();
			for (int j = 0; j<shapeCount; j++)
			{

				double t = (allShapes[j])->rayIntersectsShape(inc);

				if (t > 0 && t<mint)
				{
					intersectedObject = allShapes[j];
					mint = t;
					break;
				}
			}

			if (mint < 50000)continue;

			//Vector3d incidentRay(*(l.allLights[i]),point);

			//Vector3d normal(center, point);


			

			//Vector3d reflection = incidentRay - normal * 2 * (incidentRay.dot(normal));
			double lambert = incidentRay.dot(normal);

			Vector3d viewer(point, ray.origin);
			//Vector3d viewer(ray.origin, point);
			viewer = viewer.normalize();
			Vector3d halfVector = incidentRay + viewer;
			halfVector = halfVector.normalize();
			double phong = halfVector.dot(normal);


			Color diff = primaryColor*(diffuseCoeff*(lambert > 0 ? lambert : 0));
			double specPart = pow((phong > 0 ? phong : 0), specExp);
			Color spec = primaryColor*(specCoeff*specPart);

			//printf("%lf %lf\n", lambert, phong);

			color = color + (diff + spec)*(1.0 / lighCount);
		}

		color = color + primaryColor*ambientCoeff;

		return color;
	}

	bool pointOnCheckerBoard(Vector3d point)
	{
		if (point.x > range || point.x<-range || point.z>range || point.z < -range)return false;
		else return true;
	}

	Vector3d getNormal(Vector3d point, Ray ray){
		return Vector3d(0, 1, 0);
	}

};
#endif