#ifndef RAY_H
#define RAY_H

#include "Vector3d.h"

class Ray{

public:
	Vector3d origin;
	Vector3d direction;

	int intesects;

	Ray(){
		intesects = -1;
	}

	Ray(Vector3d or, Vector3d dir)
	{
		origin = or; 
		direction = dir;
		intesects = -1;
	}

};

#endif