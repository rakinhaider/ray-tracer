


#include "CheckerBoard.h"
#include "Sphere.h"
#include "Color.h"
#include "Triangle.h"
#include "Cylinder.h"
#include "Scenerio.h"
#include "Ray.h"
#include "bitmap_image.h"
#include "Lights.h"



#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<windows.h>


#ifndef GLUT_H
#define GLUT_H
#include<GL/glut.h>
#endif


#define BLACK 0, 0, 0
#define M_PI 3.1415923


/****************************************************************
/Function Prototype
*****************************************************************/
void drawGrid();
void readScenario();
void generateImage();
Color getPixelColor(int depth, Ray ray, Lights lght);
int canDrawGrid;

double upX, upY, upZ;
double rollAngle;
double cameraRadiusX;
double cameraAngleX;
double cameraRadiusY;
double cameraAngleY;
double cameraRadiusZ;
double cameraAngleZ;

double cameraHeight;
double cameraRadius;

double cameraForward;
double cameraAlong;
double cameraUp;

double lookX, lookY, lookZ;

double lightIntensity;
double lightAngle;

double lightColorR;
double lightColorG;
double lightColorB;

vector<Vector3d> gridPoints[1024];
vector<Ray> rays;

int height;
int width;
int depth;
int pixel;


double rectAngle;	//in degree

//bool canDrawGrid;
double cameraAngle;
Scenerio sc;
Lights lght;

/****************************************************************
/main functions openGL
*****************************************************************/

void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(BLACK, 0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera is looking?
	//3. Which direction is the camera's UP direction?

	//instead of CONSTANT information, we will define a circular path.
	//gluLookAt(-30,-30,50,	0,0,0,	0,0,1);

	//gluLookAt(cameraRadius*cos(cameraAngle), cameraRadius*sin(cameraAngle), cameraHeight,		0,0,0,		0,0,1);

	/*cameraX=cameraRadius*cos(cameraAngle);
	cameraY=cameraRadius*sin(cameraAngle);

	if(changeLookAt==1){
	lookAtX=cameraRadius*cos(cameraAxisAngle)+cameraX;
	lookAtY=cameraRadius*sin(cameraAxisAngle)+cameraY;
	changeLookAt=0;
	}

	gluLookAt(cameraX, cameraY, cameraHeight,	lookAtX,lookAtY,cameraLookAtHeight,	0,0,1);*/






	/**********************************
	taj mahal camera
	**********************************/
	gluLookAt(cameraForward, cameraAlong, cameraUp, lookX, lookY, lookZ, upX, upY, upZ);

	//NOTE: the camera still CONSTANTLY looks at the center
	// cameraAngle is in RADIAN, since you are using inside COS and SIN

	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);

	glPushMatrix(); {
		glTranslatef(0, 0, -5);
		//drawGrid();
	}glPopMatrix();


	/****************************
	/ Add your objects from here
	****************************/
	//moving light source


	drawGrid();



		
	for (vector<Shape*>::iterator it = sc.allShapes.begin(); it != sc.allShapes.end(); ++it)
	{
		(*it)->draw(); //break;
	}

	for (int i = 0; i < pixel; i++)
	{
		for (int j = 0; j < gridPoints[i].size(); j++)
		{
			//gridPoints[i][j].print();
			glPushMatrix(); {

				if (rays[i*pixel + j].intesects==-1)glColor3f(1, 0, 0);
				else glColor3f(1, 1, 0);
				glTranslatef(gridPoints[i][j].x, gridPoints[i][j].y, gridPoints[i][j].z);
				//glutSolidSphere(1.0 / pixel, 50, 50);
			}glPopMatrix();
		}
		//printf("\n");
	}

	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}

void init(){
	//codes for initialization
	/*cameraAngle = 0;	//// init the cameraAngle
	cameraAngleDelta = 0.002;
	globeAngle=0;
	canDrawGrid = true;
	cameraHeight = 150;
	cameraRadius = 150;
	*/

	/************************
	camera initialization
	************************/
	//codes for initialization
	cameraAngle = 0;	//// init the cameraAngle
	//cameraAngleDelta = 0.002;
	rectAngle = 0;
	canDrawGrid = true;
	cameraHeight = 0;
	cameraRadius = 10;

	cameraForward = 0;
	cameraAlong = 10;
	cameraUp = 80;

	lookX = 0;
	lookY = 10;
	lookZ = 0;

	upX = 0;
	upY = 1;
	upZ = 0;
	rollAngle = 0;
	cameraRadiusX = 50;
	cameraRadiusY = 50;
	cameraRadiusZ = 50;
	cameraAngleX = 180 * 3.1416 / 180;
	cameraAngleY = 0;
	cameraAngleZ = (180 - atan(10.0 / 50))*3.1416 / 180;

	lightIntensity = .5;
	lightAngle = 0;

	lightColorR = 0;
	lightColorG = 0;
	lightColorB = 1;

	

	
	//clear the screen
	glClearColor(BLACK, 0);
	
	
	//set the shading model
	GLfloat diffusePoint[] = { 1, 1, 1, 1.0 }; //Color (0.5, 0.5, 0.5)
	GLfloat position[] = { lght.allLights[0]->x, lght.allLights[0]->y, lght.allLights[0]->z, 1.0 }; //Positioned at (-10, -10, 5)
	glLightfv(GL_LIGHT1, GL_DIFFUSE, diffusePoint); //
	glLightfv(GL_LIGHT1, GL_POSITION, position);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHTING);

	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	//glColorMaterial(GL_FRONT,GL_SPECULAR);
	glEnable(GL_COLOR_MATERIAL);


	/************************
	/ set-up projection here
	************************/


	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);

	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(70, 1, 10, 10000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance

	/*
	glShadeModel(GL_SMOOTH);

	GLfloat lmodel_ambient[] = { 1, 1, 1, 1.0 }; //color of the global ambient light
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

	glEnable(GL_NORMALIZE); //Automatically normalize normals needed by the illumination model
	glEnable(GL_LIGHTING);
	*/
}
void animate(){
	//codes for any changes in Camera
	//if(stopCamera==0){
	//	cameraAngle += cameraAngleDelta;	// camera will rotate at 0.002 radians per frame.	// keep the camera steady NOW!!
	//}

	//globeAngle+=1;
	//if(globeAngle==360){
	//	globeAngle=0;
	//}

	//codes for any changes in Models

	//rectAngle -= 1;

	
	rectAngle -= 1;

	//MISSING SOMETHING? -- YES: add the following
	glutPostRedisplay();	//this will call the display AGAIN
}

void keyboardListener(unsigned char key, int x, int y){

	double dirX, dirY, dirZ;
	double alongX, alongY, alongZ;
	//double newLookX,newLookY,newLookZ;
	double modForward, modAlong, modUp, r;
	double upPointX, upPointY, upPointZ;
	double angle;

	switch (key){

	case '1':
		//cameraAngleX=cameraAngleX+.01;
		//lookX=cameraForward+cameraRadius*cos(cameraAngleX);
		//lookY= cameraAlong+ cameraRadius*sin(cameraAngleX);

		r = sqrt((cameraForward - lookX)*(cameraForward - lookX) + (cameraAlong - lookY)*(cameraAlong - lookY) + (cameraUp - lookZ)*(cameraUp - lookZ));

		dirX = cameraForward - lookX;
		dirY = cameraAlong - lookY;
		dirZ = cameraUp - lookZ;

		alongX = -dirY*upZ + dirZ*upY;
		alongY = -dirZ*upX + dirX*upZ;
		alongZ = -dirX*upY + dirY*upX;

		modAlong = 1 / sqrt(alongX*alongX + alongY*alongY + alongZ*alongZ);
		modForward = 1 / sqrt(dirX*dirX + dirY*dirY + dirZ*dirZ);

		angle = 180 * 3.1416 / 180 + .1;

		lookX = cameraForward + r*dirX*modForward*cos(angle) + r*alongX*modAlong*sin(angle);
		lookY = cameraAlong + r*dirY*modForward*cos(angle) + r*alongY*modAlong*sin(angle);
		lookZ = cameraUp + r*dirZ*modForward*cos(angle) + r*alongZ*modAlong*sin(angle);

		//printf("%lf\n",r);
		//printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
		//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
		//printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);

		break;

	case '2':
		r = sqrt((cameraForward - lookX)*(cameraForward - lookX) + (cameraAlong - lookY)*(cameraAlong - lookY) + (cameraUp - lookZ)*(cameraUp - lookZ));

		dirX = cameraForward - lookX;
		dirY = cameraAlong - lookY;
		dirZ = cameraUp - lookZ;

		alongX = -dirY*upZ + dirZ*upY;
		alongY = -dirZ*upX + dirX*upZ;
		alongZ = -dirX*upY + dirY*upX;

		modAlong = 1 / sqrt(alongX*alongX + alongY*alongY + alongZ*alongZ);
		modForward = 1 / sqrt(dirX*dirX + dirY*dirY + dirZ*dirZ);

		angle = 180 * 3.1416 / 180 - .1;

		lookX = cameraForward + r*dirX*modForward*cos(angle) + r*alongX*modAlong*sin(angle);
		lookY = cameraAlong + r*dirY*modForward*cos(angle) + r*alongY*modAlong*sin(angle);
		lookZ = cameraUp + r*dirZ*modForward*cos(angle) + r*alongZ*modAlong*sin(angle);

		//printf("%lf\n",r);
		//printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
		//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
		//printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);
		break;

	case '3':
		//cameraAngleZ=cameraAngleZ+.01;
		//lookX=cameraForward+cameraRadius*cos(cameraAngleZ);
		//lookZ=cameraUp+cameraRadius*sin(cameraAngleZ);

		r = sqrt((cameraForward - lookX)*(cameraForward - lookX) + (cameraAlong - lookY)*(cameraAlong - lookY) + (cameraUp - lookZ)*(cameraUp - lookZ));

		dirX = cameraForward - lookX;
		dirY = cameraAlong - lookY;
		dirZ = cameraUp - lookZ;

		alongX = -dirY*upZ + dirZ*upY;
		alongY = -dirZ*upX + dirX*upZ;
		alongZ = -dirX*upY + dirY*upX;

		modForward = 1 / sqrt(dirX*dirX + dirY*dirY + dirZ*dirZ);
		modUp = 1 / sqrt(upX*upX + upY*upY + upZ*upZ);
		angle = 180 * 3.1416 / 180 + .1;

		lookX = cameraForward + r*dirX*modForward*cos(angle) + r*upX*modUp*sin(angle);
		lookY = cameraAlong + r*dirY*modForward*cos(angle) + r*upY*modUp*sin(angle);
		lookZ = cameraUp + r*dirZ*modForward*cos(angle) + r*upZ*modUp*sin(angle);

		dirX = cameraForward - lookX;
		dirY = cameraAlong - lookY;
		dirZ = cameraUp - lookZ;

		upX = dirY*alongZ - dirZ*alongY;
		upY = dirZ*alongX - dirX*alongZ;
		upZ = dirX*alongY - dirY*alongX;

		modUp = 1 / sqrt(upX*upX + upY*upY + upZ*upZ);

		upX *= modUp;
		upY *= modUp;
		upZ *= modUp;

		//printf("%lf\n",dirX*upX+dirY*upY+dirZ*upZ);
		//printf("%lf\n",dirX*alongX+dirY*alongY+dirZ*alongZ);
		//printf("%lf\n",alongX*upX+alongY*upY+alongZ*upZ);


		//printf("%lf\n",r);
		//printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
		//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
		//printf("%lf,%lf,%lf\n",upX,upY,upZ);
		//printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);
		break;

	case '4':
		//cameraAngleZ=cameraAngleZ-.01;
		//lookX=cameraForward+cameraRadius*cos(cameraAngleZ);
		//lookZ=cameraUp+cameraRadius*sin(cameraAngleZ);

		r = sqrt((cameraForward - lookX)*(cameraForward - lookX) + (cameraAlong - lookY)*(cameraAlong - lookY) + (cameraUp - lookZ)*(cameraUp - lookZ));

		dirX = cameraForward - lookX;
		dirY = cameraAlong - lookY;
		dirZ = cameraUp - lookZ;

		alongX = -dirY*upZ + dirZ*upY;
		alongY = -dirZ*upX + dirX*upZ;
		alongZ = -dirX*upY + dirY*upX;

		modForward = 1 / sqrt(dirX*dirX + dirY*dirY + dirZ*dirZ);
		modUp = 1 / sqrt(upX*upX + upY*upY + upZ*upZ);
		angle = 180 * 3.1416 / 180 - .1;

		lookX = cameraForward + r*dirX*modForward*cos(angle) + r*upX*modUp*sin(angle);
		lookY = cameraAlong + r*dirY*modForward*cos(angle) + r*upY*modUp*sin(angle);
		lookZ = cameraUp + r*dirZ*modForward*cos(angle) + r*upZ*modUp*sin(angle);

		dirX = cameraForward - lookX;
		dirY = cameraAlong - lookY;
		dirZ = cameraUp - lookZ;

		upX = dirY*alongZ - dirZ*alongY;
		upY = dirZ*alongX - dirX*alongZ;
		upZ = dirX*alongY - dirY*alongX;

		modUp = 1 / sqrt(upX*upX + upY*upY + upZ*upZ);

		upX *= modUp;
		upY *= modUp;
		upZ *= modUp;

		//printf("%lf\n",dirX*upX+dirY*upY+dirZ*upZ);
		//printf("%lf\n",dirX*alongX+dirY*alongY+dirZ*alongZ);
		//printf("%lf\n",alongX*upX+alongY*upY+alongZ*upZ);


		//printf("%lf\n",r);
		//printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
		//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
		//printf("%lf,%lf,%lf\n",upX,upY,upZ);
		//printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);

		break;

	case '5':
		/*rollAngle=rollAngle+.01;
		upX=cos(rollAngle);
		upZ=sin(rollAngle);
		printf("%lf,%lf\n",upX,upZ);*/

		r = sqrt((cameraForward - lookX)*(cameraForward - lookX) + (cameraAlong - lookY)*(cameraAlong - lookY) + (cameraUp - lookZ)*(cameraUp - lookZ));

		dirX = cameraForward - lookX;
		dirY = cameraAlong - lookY;
		dirZ = cameraUp - lookZ;

		alongX = -dirY*upZ + dirZ*upY;
		alongY = -dirZ*upX + dirX*upZ;
		alongZ = -dirX*upY + dirY*upX;

		modAlong = 1 / sqrt(alongX*alongX + alongY*alongY + alongZ*alongZ);
		modUp = 1 / sqrt(upX*upX + upY*upY + upZ*upZ);
		angle = 0 * 3.1416 / 180 + .01;

		upPointX = cameraForward + r*upX*modUp*cos(angle) + r*alongX*modAlong*sin(angle);
		upPointY = cameraAlong + r*upY*modUp*cos(angle) + r*alongY*modAlong*sin(angle);
		upPointZ = cameraUp + r*upZ*modUp*cos(angle) + r*alongZ*modAlong*sin(angle);

		/*dirX=cameraForward-lookX;
		dirY=cameraAlong-lookY;
		dirZ=cameraUp-lookZ;

		upX=dirY*alongZ-dirZ*alongY;
		upY=dirZ*alongX-dirX*alongZ;
		upZ=dirX*alongY-dirY*alongX;*/

		upX = upPointX - cameraForward;
		upY = upPointY - cameraAlong;
		upZ = upPointZ - cameraUp;

		modUp = 1 / sqrt(upX*upX + upY*upY + upZ*upZ);

		upX *= modUp;
		upY *= modUp;
		upZ *= modUp;

		//printf("%lf\n",dirX*upX+dirY*upY+dirZ*upZ);
		//printf("%lf\n",dirX*alongX+dirY*alongY+dirZ*alongZ);
		//printf("%lf\n",alongX*upX+alongY*upY+alongZ*upZ);


		//printf("%lf\n",r);
		//printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
		//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
		//printf("%lf,%lf,%lf\n",upX,upY,upZ);
		//printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);

		break;

	case '6':
		r = sqrt((cameraForward - lookX)*(cameraForward - lookX) + (cameraAlong - lookY)*(cameraAlong - lookY) + (cameraUp - lookZ)*(cameraUp - lookZ));

		dirX = cameraForward - lookX;
		dirY = cameraAlong - lookY;
		dirZ = cameraUp - lookZ;

		alongX = -dirY*upZ + dirZ*upY;
		alongY = -dirZ*upX + dirX*upZ;
		alongZ = -dirX*upY + dirY*upX;

		modAlong = 1 / sqrt(alongX*alongX + alongY*alongY + alongZ*alongZ);
		modUp = 1 / sqrt(upX*upX + upY*upY + upZ*upZ);
		angle = 0 * 3.1416 / 180 - .01;

		upPointX = cameraForward + r*upX*modUp*cos(angle) + r*alongX*modAlong*sin(angle);
		upPointY = cameraAlong + r*upY*modUp*cos(angle) + r*alongY*modAlong*sin(angle);
		upPointZ = cameraUp + r*upZ*modUp*cos(angle) + r*alongZ*modAlong*sin(angle);

		/*dirX=cameraForward-lookX;
		dirY=cameraAlong-lookY;
		dirZ=cameraUp-lookZ;

		upX=dirY*alongZ-dirZ*alongY;
		upY=dirZ*alongX-dirX*alongZ;
		upZ=dirX*alongY-dirY*alongX;*/

		upX = upPointX - cameraForward;
		upY = upPointY - cameraAlong;
		upZ = upPointZ - cameraUp;

		modUp = 1 / sqrt(upX*upX + upY*upY + upZ*upZ);

		upX *= modUp;
		upY *= modUp;
		upZ *= modUp;

		//printf("%lf\n",dirX*upX+dirY*upY+dirZ*upZ);
		//printf("%lf\n",dirX*alongX+dirY*alongY+dirZ*alongZ);
		//printf("%lf\n",alongX*upX+alongY*upY+alongZ*upZ);


		//printf("%lf\n",r);
		//printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
		//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
		//printf("%lf,%lf,%lf\n",upX,upY,upZ);
		//printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);
		break;

	case '8':	//toggle grids
		canDrawGrid = 1 - canDrawGrid;
		break;


	case 'w':
		lightColorR = lightColorG = lightColorB = 1;
		break;
	case 'b':
		lightColorR = lightColorG = 0;
		lightColorB = 1;
		break;


	case 's':
		lightAngle += .1;
		break;

	case 'a':
		lightAngle -= .1;
		break;


	case 'd':
		lightIntensity = .5;
		break;
	case 'n':
		lightIntensity = .2;
		break;

	case 'x':

		break;

	case 'c':

		break;

	case 'v':

		break;



	case 'r':
		cameraAngle = 0;	//// init the cameraAngle
		//cameraAngleDelta = 0.002;
		rectAngle = 0;
		canDrawGrid = true;
		cameraHeight = 0;
		cameraRadius = 10;

		cameraForward = 40;
		cameraAlong = 250;
		cameraUp = 70;

		lookX = 0;
		lookY = 0;
		lookZ = 0;

		upX = 0;
		upY = 0;
		upZ = 1;
		rollAngle = 0;
		cameraRadiusX = 50;
		cameraRadiusY = 50;
		cameraRadiusZ = 50;
		cameraAngleX = 180 * 3.1416 / 180;
		cameraAngleY = 0;
		cameraAngleZ = (180 - atan(10.0 / 50))*3.1416 / 180;
		break;
	case 27:	//ESCAPE KEY -- simply exit
		exit(0);
		break;

	default:
		break;
	}
	GLfloat lmodel_ambient[] = { lightIntensity, lightIntensity, lightIntensity, 1.0 }; //color of the global ambient light
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

	glDisable(GL_LIGHT1);
	GLfloat diffusePoint[] = { lightColorR, lightColorG, lightColorB };
	glLightfv(GL_LIGHT1, GL_DIFFUSE, diffusePoint);

	glEnable(GL_LIGHT1);
}

void specialKeyListener(int key, int x, int y){

	double dirX, dirY, dirZ;
	double alongX, alongY, alongZ;
	double newLookX, newLookY, newLookZ;
	double normalize;
	double speed = 5;
	switch (key){
	case GLUT_KEY_DOWN:		//down arrow key
		//cameraForward=cameraForward+5;
		//lookX+=5;
		//printf("%lf,%lf,%lf %lf,%lf,%lf %lf,%lf,%lf\n",dirX,dirY,dirZ,lookX,lookY,lookZ,cameraForward,cameraAlong,cameraUp);

		dirX = cameraForward - lookX;
		dirY = cameraAlong - lookY;
		dirZ = cameraUp - lookZ;

		normalize = 1 / sqrt(dirX*dirX + dirY*dirY + dirZ*dirZ);

		newLookX = lookX + normalize*dirX*speed;
		newLookY = lookY + normalize*dirY*speed;
		newLookZ = lookZ + normalize*dirZ*speed;

		cameraForward = cameraForward + normalize*dirX*speed;
		cameraAlong = cameraAlong + normalize*dirY*speed;
		cameraUp = cameraUp + normalize*dirZ*speed;

		lookX = newLookX;
		lookY = newLookY;
		lookZ = newLookZ;

		break;
	case GLUT_KEY_UP:		// up arrow key
		dirX = cameraForward - lookX;
		dirY = cameraAlong - lookY;
		dirZ = cameraUp - lookZ;
		normalize = 1 / sqrt(dirX*dirX + dirY*dirY + dirZ*dirZ);

		newLookX = lookX - normalize*dirX*speed;
		newLookY = lookY - normalize*dirY*speed;
		newLookZ = lookZ - normalize*dirZ*speed;

		cameraForward = cameraForward - normalize*dirX*speed;
		cameraAlong = cameraAlong - normalize*dirY*speed;
		cameraUp = cameraUp - normalize*dirZ*speed;

		lookX = newLookX;
		lookY = newLookY;
		lookZ = newLookZ;

		break;

	case GLUT_KEY_RIGHT:
		//cameraAlong=cameraAlong+5;
		//lookY+=5;

		//printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);

		dirX = cameraForward - lookX;
		dirY = cameraAlong - lookY;
		dirZ = cameraUp - lookZ;

		alongX = dirY*upZ - dirZ*upY;
		alongY = dirZ*upX - dirX*upZ;
		alongZ = dirX*upY - dirY*upX;

		normalize = 1 / sqrt(alongX*alongX + alongY*alongY + alongZ*alongZ);

		cameraForward = cameraForward - normalize*alongX;
		cameraAlong = cameraAlong - normalize*alongY;
		cameraUp = cameraUp - normalize*alongZ;

		lookX = lookX - normalize*alongX;
		lookY = lookY - normalize*alongY;
		lookZ = lookZ - normalize*alongZ;

		//printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
		//printf("%lf,%lf,%lf\n",lookX,lookY,lookZ);

		break;
	case GLUT_KEY_LEFT:
		//cameraAlong=cameraAlong-5;
		//lookY-=5;

		dirX = cameraForward - lookX;
		dirY = cameraAlong - lookY;
		dirZ = cameraUp - lookZ;

		alongX = dirY*upZ - dirZ*upY;
		alongY = dirZ*upX - dirX*upZ;
		alongZ = dirX*upY - dirY*upX;

		normalize = 1 / sqrt(alongX*alongX + alongY*alongY + alongZ*alongZ);

		cameraForward = cameraForward + normalize*alongX;
		cameraAlong = cameraAlong + normalize*alongY;
		cameraUp = cameraUp + normalize*alongZ;

		lookX = lookX + normalize*alongX;
		lookY = lookY + normalize*alongY;
		lookZ = lookZ + normalize*alongZ;

		//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
		//printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
		//printf("%lf,%lf,%lf\n",lookX,lookY,lookZ);
		break;

	case GLUT_KEY_PAGE_UP:
		//cameraUp=cameraUp+2;
		//lookZ+=2;

		normalize = 1 / sqrt(upX*upX + upY*upY + upZ*upZ);

		cameraForward = cameraForward + normalize*upX;
		cameraAlong = cameraAlong + normalize*upY;
		cameraUp = cameraUp + normalize*upZ;

		lookX = lookX + normalize*upX;
		lookY = lookY + normalize*upY;
		lookZ = lookZ + normalize*upZ;
		//printf("%lf,%lf,%lf\n",upX,upY,upZ);
		//printf("%lf\n",normalize);
		//printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
		//printf("%lf,%lf,%lf\n",lookX,lookY,lookZ);

		break;
	case GLUT_KEY_PAGE_DOWN:
		//cameraUp=cameraUp-2;
		//lookZ-=2;

		normalize = 1 / sqrt(upX*upX + upY*upY + upZ*upZ);

		cameraForward = cameraForward - normalize*upX;
		cameraAlong = cameraAlong - normalize*upY;
		cameraUp = cameraUp - normalize*upZ;

		lookX = lookX - normalize*upX;
		lookY = lookY - normalize*upY;
		lookZ = lookZ - normalize*upZ;
		//printf("%lf,%lf,%lf\n",upX,upY,upZ);
		//printf("%lf\n",normalize);
		//printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
		//printf("%lf,%lf,%lf\n",lookX,lookY,lookZ);

		break;

	case GLUT_KEY_INSERT:
		break;

	case GLUT_KEY_HOME:
		cameraAngleX += .01;
		cameraAngleY += .01;
		break;
	case GLUT_KEY_END:
		cameraAngleX -= .01;
		cameraAngleY -= .01;
		break;

	default:
		break;
	}
}

void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
	switch (button){
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN){
			// 2 times?? in ONE click? -- solution is checking DOWN or UP
			//cameraAngleDelta = -cameraAngleDelta;	

			generateImage();
			
		}
		break;

	case GLUT_RIGHT_BUTTON:
		//........
		break;

	case GLUT_MIDDLE_BUTTON:
		//........
		break;

	default:
		break;
	}
}



int main(int argc, char **argv){
	readScenario();

	
	glutInit(&argc, argv);
	height = 500;
	width = 500;
	glutInitWindowSize(height, width);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("Ray Tracing");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	//ADD keyboard listeners:
	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);

	//ADD mouse listeners:
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}


void drawGrid(){
	int i;

	//WILL draw grid IF the "canDrawGrid" is true:

	if (canDrawGrid == 1){
		glColor3f(0.3, 0.3, 0.3);	//grey
		glBegin(GL_LINES); {
			for (i = -10; i <= 10; i++){

				if (i == 0)
					continue;	//SKIP the MAIN axes

				//lines parallel to Y-axis
				glVertex3f(i * 10, 0,  - 100);
				glVertex3f(i * 10, 0,100);

				//lines parallel to X-axis
				glVertex3f(-100, 0, i * 10);
				glVertex3f(100, 0 ,i * 10);


			}
		}glEnd();
		// draw the two AXES
		//glColor3f(1, 1, 1);	//100% white
		glColor3f(0.3, 0.3, 0.3);
		glBegin(GL_LINES); {
			//Y axis
			glColor3f(1, 0, 0);
			glVertex3f(0, 0, -150);	// intentionally extended to -150 to 150, no big deal
			glVertex3f(0, 0, 150);

			//X axis
			glColor3f(0, 1, 0);
			glVertex3f(-150, 0, 0);
			glVertex3f(150, 0, 0);
		}glEnd();
	}
}



void readScenario(){

	
	char s[50];
	char shapeType[50];
	//FILE *f = fopen("Spec.txt", "r");
	//fscanf(f, "%d", &depth);
	freopen("Spec.txt", "r", stdin);
	scanf("%s %d",s, &depth);
	printf("%s %d\n", s,depth);
	scanf("%s %d", s, &pixel);
	printf("%s %d\n",s, pixel);
	
	gets(s);

	while (scanf("%s",s)==1){
		if (strcmp(s, "objStart") == 0)
		{
			scanf("%s", shapeType);
			printf("%s %s\n", s, shapeType);

			if (strcmp(shapeType, "CHECKERBOARD") == 0)
			{
				CheckerBoard* chckBord= new CheckerBoard();
				chckBord->input();
				sc.insertNewShape(chckBord);
			}
			else if (strcmp(shapeType, "SPHERE") == 0)
			{
				Sphere* sphere= new Sphere();
				sphere->input();
				sc.insertNewShape(sphere);
			}
			else if (strcmp(shapeType, "TRIANGLE") == 0)
			{
				Triangle* triangle= new Triangle();
				triangle->input();
				//triangle->draw();
				sc.insertNewShape(triangle);
			}
			else if (strcmp(shapeType, "CYLINDER") == 0)
			{
				Cylinder* cylinder=new Cylinder();
				cylinder->input();
				sc.insertNewShape(cylinder);
			}
		}
		else if (strcmp(s, "light") == 0)
		{
			Vector3d* v=new Vector3d();
			v->dot(Vector3d(0, 0, 0));
			v->input();

			lght.insertNewLight(v);

			
		}

	}

	printf("%d", sc.allShapes.size());

	/*for (int i = 0; i < sc.allShapes.size(); i++)
	{
		Shape *s = &sc.allShapes[i];
		CheckerBoard *chck = static_cast<CheckerBoard *>(s);
		chck->print();
	}*/

	for (vector<Shape*>::iterator it = sc.allShapes.begin(); it != sc.allShapes.end(); ++it)
	{
		(*it)->print();
	}

	
}

void traceRay(Ray r){

}

void generateImage(){
	

	bitmap_image image(pixel,pixel);

	double*** pixels = new double**[pixel];

	for (int i = 0; i < pixel; i++)
	{
		pixels[i] = new double *[pixel];
		for (int j = 0; j < pixel; j++)
		{
			pixels[i][j] = new double[3];
		}
	}

	double N = 10;//(height/2)/tan(75*3.1416/180);

	
	Vector3d lookAtPosition(lookX, lookY, lookZ);
	Vector3d cameraPosition(cameraForward, cameraAlong, cameraUp);
	Vector3d forwardVector(cameraPosition,lookAtPosition);
	Vector3d alongVector;
	Vector3d upVector(upX, upY, upZ);

	//alongVector = upVector.cross(forwardVector);//forwardVector.cross(upVector);
	alongVector = forwardVector.cross(upVector);

	forwardVector = forwardVector.normalize();
	alongVector = alongVector.normalize();
	upVector = upVector.normalize();
	forwardVector.print();
	alongVector.print();
	upVector.print();
	Vector3d cornerPixelCenter;

	double w, h;
	h = 2*N*tan(75 / 2 * 3.1416 / 180);
	w = h*(width*1.0/height);
	
	//printf("%lf,%lf,%lf",cornerPixelCenter.x,cornerPixelCenter.y,cornerPixelCenter.z);
	rays.clear();
	for (int  i = 0; i < pixel; i++)
	{
		//gridPoints[i].clear();
		for (int j = 0; j < pixel; j++)
		{
			Vector3d cornerPixelCenter;

			cornerPixelCenter = cameraPosition + forwardVector*N + upVector*(h/2) - alongVector*(w/2) -upVector*(i*h/pixel)+alongVector*(j*w/pixel);
			//cornerPixelCenter.print();
			//gridPoints[i].push_back(cornerPixelCenter);
			Vector3d direction(cameraPosition, cornerPixelCenter);
			//direction = direction.normalize();
			Ray ray(cameraPosition, direction);
			//rays.push_back(ray);

			pixels[i][j][0] = 0;
			pixels[i][j][1] = 0;
			pixels[i][j][2] = 0;
			bool intersected = false;
			/*double mint = 500000;
			Shape *intersectedObject=NULL;
			for (vector<Shape*>::iterator it = sc.allShapes.begin(); it != sc.allShapes.end(); ++it)
			{

				double t = (*it)->rayIntersectsShape(ray);
								
				if (t >= 0 && t<mint)
				{
					intersectedObject = *it;
					mint = t;
				}
			}

			if (mint < 50000)
			{
				Vector3d intersectingPoint(ray.origin, mint, ray.direction);
				Color c = intersectedObject->getColorAtPoint(ray, intersectingPoint, lght);
				pixels[i][j][0] = (c.r);
				pixels[i][j][1] = (c.g);
				pixels[i][j][2] = (c.b);

				printf("%lf %lf %lf\n", c.r, c.g, c.b);

				ray.intesects = 1;
			}*/

			Color c=getPixelColor(depth,ray,lght);
			pixels[i][j][0] = (c.r);
			pixels[i][j][1] = (c.g);
			pixels[i][j][2] = (c.b);

			//rays.push_back(ray);
			
		}
		if (i % 100 == 0)printf("%d\n",i);
		//printf("\n");
	}

	for (int i = 0; i < pixel; i++)
	{
		for (int j = 0; j < pixel; j++)
		{
			image.set_pixel(j, i, (byte)(pixels[i][j][0] * 255), (byte)(pixels[i][j][1] * 255), (byte)(pixels[i][j][2] * 255));
		}
	}

	image.save_image("output.png");
	printf("Done");

}

Color getPixelColor(int recDepth, Ray ray, Lights lght){
	
	double mint = 500000;
	Shape *intersectedObject = NULL;
	/*for (vector<Shape*>::iterator it = sc.allShapes.begin(); it != sc.allShapes.end(); ++it)
	{

		double t = (*it)->rayIntersectsShape(ray);

		if (t > 0 && t<mint)
		{
			intersectedObject = *it;
			mint = t;
		}
	}*/

	int shapeCount = sc.allShapes.size();
	for (int i = 0; i<shapeCount;i++)
	{

		double t = (sc.allShapes[i])->rayIntersectsShape(ray);

		if (t > 0 && t<mint)
		{
			intersectedObject = sc.allShapes[i];
			mint = t;
		}
	}

	if (mint < 50000)
	{
		
		if(intersectedObject->objectType!=3)mint = mint - .005;

		if (recDepth == 1)
		{
			Vector3d intersectingPoint(ray.origin, mint, ray.direction);
			Color objectCol = intersectedObject->getColorAtPoint(ray, intersectingPoint, lght,sc.allShapes);
			return objectCol;
			
		}
		else
		{
			/*Vector3d intersectingPoint(ray.origin, mint, ray.direction);
			Color c = intersectedObject->getColorAtPoint(ray, intersectingPoint, lght);
			pixels[i][j][0] = (c.r);
			pixels[i][j][1] = (c.g);
			pixels[i][j][2] = (c.b);

			printf("%lf %lf %lf\n", c.r, c.g, c.b);

			ray.intesects = 1;*/

			Vector3d intersectingPoint(ray.origin, mint, ray.direction);
			Vector3d normal = intersectedObject->getNormal(intersectingPoint, ray);
			Vector3d incidentRay = ray.direction.normalize();
			Vector3d reflection = incidentRay - normal * 2 * (incidentRay.dot(normal));

			Ray refRay(intersectingPoint, reflection);
			Color refC = getPixelColor(recDepth - 1, refRay, lght);
			Color objectCol = intersectedObject->getColorAtPoint(ray, intersectingPoint, lght,sc.allShapes);
			
			double R = 1;
			Color refractedC(0,0,0);
			if (intersectedObject->objectType == 3 )
			{
				double meo1 = 1;
				double meo2 = intersectedObject->refractiveIndex;
				Vector3d fParallel = incidentRay - normal * (incidentRay.dot(normal));
				fParallel = fParallel * (meo1 / meo2);

				double coeff = (incidentRay*-1).dot(normal);
				coeff = (1 - coeff)*(1 - coeff)*(meo1 / meo2)*(meo1 / meo2);
				coeff = 1 - coeff;
				coeff = sqrt(coeff);
				coeff = -coeff;
				Vector3d fPerpendicular = normal*coeff;

				Vector3d refractedDir = fParallel + fPerpendicular;
				Ray refractedRay(intersectingPoint, refractedDir);
				refractedC = getPixelColor(recDepth - 1, refractedRay, lght);
				double R0;

				R0 = (meo1 - meo2) / (meo1 + meo2);
				R0 = pow(R0, 2);
				if (meo1 < meo2)
				{
					double cosThetaI = (incidentRay*-1).dot(normal);
					if (cosThetaI<0)cosThetaI = -cosThetaI;
					R = R0 + (1 - R0)*pow((1 - cosThetaI), 5);
				}
				else if (meo1>meo2)
				{
					double cosThetaF = (normal*-1).dot(refractedDir);
					if (cosThetaF < 0)cosThetaF = -cosThetaF;
					R = R0 + (1 - R0)*pow((1 - cosThetaF), 5);
				}
				else R = 1;

				//finalColor = refractedC*R + finalColor*(1 - R);
			}

			//Color finalColor = objectCol +(refC*(intersectedObject->refCoeff))*(R)+refractedC*(1 - R);
			Color finalColor = objectCol + (refC*R+refractedC*(1 - R))*(intersectedObject->refCoeff);

			if (finalColor.r>1 || finalColor.g > 1 || finalColor.b > 1)
			{
				return objectCol + (refC*(intersectedObject->refCoeff));
			}

			return finalColor;
			
		}
	
	}
	else return Color(0, 0, 0);
}