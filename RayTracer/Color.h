
#ifndef COLOR_H
#define COLOR_H

class Color{

public:
	
	double r, g, b;

	Color(){}

	void setColor(Color c)
	{
		r = c.r;
		g = c.g;
		b = c.b;
	}

	Color(double red,double green,double blue){
		r = red;
		g = green;
		b = blue;
	}

	Color operator+(const Color c)
	{
		Color newc;
		newc.r = this->r + c.r;
		newc.g = this->g + c.g;
		newc.b = this->b + c.b;

		return newc;
	}

	Color operator-(const Color c)
	{
		Color newc;
		newc.r = this->r - c.r;
		newc.g = this->g - c.g;
		newc.b = this->b - c.b;

		return newc;
	}

	Color operator=(const Color c)
	{
		//Vector3d newv;
		r = c.r;
		g = c.g;
		b = c.b;
		return (*this);
	}

	Color operator*(double d)
	{
		Color newc;
		newc.r = r*d;
		newc.g = g*d;
		newc.b = b*d;
		return newc;
	}

	Color operator*(Color c)
	{
		Color newc;
		newc.r = r*c.r;
		newc.g = g*c.g;
		newc.b = b*c.b;
		return newc;
	}

	void print(){
		printf("Color\n");
		printf("%lf,%lf,%lf\n",r,g,b);
	}

	void input(){
		scanf("%lf %lf %lf", &r, &g, &b);
	}

};
#endif