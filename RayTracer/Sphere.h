#ifndef SPHERE_H
#define SPHERE_H

#include "Shape.h"
#include "Scenerio.h"

class Sphere :public Shape{

public:
	Vector3d center;
	double radius;
	Color c;

	Sphere(){
		refractiveIndex = -1;
		objectType = 2;
	}
	void print(){
		printf("Sphere\n");
		center.print();
		printf("%lf\n",radius);
		c.print();
	}

	void draw(){
		glPushMatrix();{
			glColor3f(c.r, c.g, c.b);
			glTranslatef(center.x, center.y, center.z);
			glutSolidSphere(radius,50,50);
		}glPopMatrix();
	}

	void input(){
		char s[50];
		while (scanf("%s", s) == 1)
		{
			if (strcmp(s, "objEnd") == 0)break;
			else if (strcmp(s, "color") == 0)
			{
				c.input();
			}
			else if (strcmp(s, "ambCoeff") == 0){
				scanf("%lf", &ambientCoeff);
			}
			else if (strcmp(s, "difCoeff") == 0){
				scanf("%lf", &diffuseCoeff);
			}
			else if (strcmp(s, "refCoeff") == 0){
				scanf("%lf", &refCoeff);
			}
			else if (strcmp(s, "specCoeff") == 0){
				scanf("%lf", &specCoeff);
			}
			else if (strcmp(s, "specExp") == 0){
				scanf("%lf", &specExp);
			}
			else if (strcmp(s, "center") == 0){
				center.input();
			}
			else if (strcmp(s, "radius") == 0){
				scanf("%lf", &radius);
			}

		}
	}
	double rayIntersectsShape(Ray ray){
		double  A, B, C;
		Vector3d temp=ray.origin-center;
		A = ray.direction.dot(ray.direction);

		B = 2*ray.direction.dot(temp);
		C = (temp).dot(temp) - radius*radius;

		double D = B*B - 4 * A*C;
		if (D< 0)return -1;
		else
		{
			if (D==0)
			{
				return -B / (2 * A);
			}

			double t1, t2;
			
			t1 = (-B + sqrt(D)) / (2 * A);
			t2 = (-B - sqrt(D)) / (2 * A);

			Vector3d intersect1(ray.origin, t1, ray.direction);
			Vector3d intersect2(ray.origin, t2, ray.direction);

			double dist1, dist2;
			dist1 = ray.origin.distance(intersect1);
			dist2 = ray.origin.distance(intersect2);

			if (dist1 > dist2)return t2;
			else return t1;

		}
	}
	Color getColorAtPoint(Ray ray, Vector3d point, Lights l, vector<Shape *> allShapes){
		int lighCount = l.allLights.size();
		Color color(0, 0, 0);

		for (int i = 0; i < lighCount; i++)
		{
			Vector3d incidentRay(point, *(l.allLights[i]));
			//Vector3d incidentRay(*(l.allLights[i]),point);

			Vector3d normal(center, point);

			incidentRay = incidentRay.normalize();
			normal = normal.normalize();

			Ray inc(*(l.allLights[i]), incidentRay);
			double mint = 500000;
			Shape *intersectedObject = NULL;
			int shapeCount = allShapes.size();
			for (int j = 0; j<shapeCount; j++)
			{

				double t = (allShapes[j])->rayIntersectsShape(inc);

				if (t > 0 && t<mint)
				{
					intersectedObject = allShapes[j];
					mint = t;
					break;
				}
			}

			if (mint < 50000)continue;

			//Vector3d reflection = incidentRay - normal * 2 * (incidentRay.dot(normal));
			double lambert = incidentRay.dot(normal);
			
			Vector3d viewer(point,ray.origin);
			//Vector3d viewer(ray.origin, point);
			viewer = viewer.normalize();
			Vector3d halfVector = incidentRay + viewer;
			halfVector = halfVector.normalize();
			double phong = halfVector.dot(normal);
			
			
			Color diff = c*(diffuseCoeff*(lambert > 0 ? lambert : 0));
			double specPart = pow((phong > 0 ? phong : 0), specExp);
			Color spec = c*(specCoeff*specPart);

			//printf("%lf %lf\n",lambert,phong);

			color = color + (diff + spec)*(1.0 / lighCount);
		}
		
		color = color + c*ambientCoeff;
		
		return color;
	}
	
	Vector3d getNormal(Vector3d point, Ray ray)
	{
		Vector3d normal(center, point);
		return normal.normalize();
	}


};
#endif