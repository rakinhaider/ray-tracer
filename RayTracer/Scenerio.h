#ifndef SCENERIO_H
#define SCENERIO_H

#include "Shape.h"
#include "Cylinder.h"
#include "Sphere.h"
#include "CheckerBoard.h"
#include "Triangle.h"

#include <vector>

using namespace std;
class Scenerio{

public:
	vector<Shape *> allShapes;
	//static vector<Vector3d *> allLights;
	
	void insertNewShape(Shape* s){
		allShapes.push_back(s);
	}

	/*void insertNewLight(Vector3d* v){
		allLights.push_back(v);
	}*/


};

#endif