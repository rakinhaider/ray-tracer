#ifndef SHAPE_H
#define SHAPE_H

#include "Lights.h"
#include "Color.h"
#include "Ray.h"
#include <stdio.h>
#include <string.h>
#include<stdlib.h>
#include<GL/glut.h>

class Shape{

public:
	double ambientCoeff;
	double diffuseCoeff;
	double specCoeff;
	double refCoeff;
	double specExp;
	int objectType;
	double refractiveIndex;

	virtual void draw(){}
	virtual void input(){}
	virtual void print(){}
	virtual double rayIntersectsShape(Ray ray){
		return -1;
	}
	virtual Color getColorAtPoint(Ray ray, Vector3d point, Lights l, vector<Shape *> allShapes){
		return Color(0, 0, 0);
	}

	virtual Vector3d getNormal(Vector3d point,Ray ray){
		return Vector3d(0, 0, 0);
	}

};
#endif