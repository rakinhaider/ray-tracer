#ifndef CYLINDER_H
#define CYLINDER_H

#include "Shape.h"

class Cylinder :public Shape{

public:
	
	double xCenter, zCenter;
	double yMax, yMin;
	double radius;
	Color c;

	Cylinder(){
		refractiveIndex = -1;
		objectType = 4;
	}

	void print(){
		printf("Cylinder\n");
		printf("%ld,%ld\n",xCenter,zCenter);
		printf("%ld,%ld\n", yMax, yMin);
		printf("%ld\n", radius);
		c.print();

	}

	void draw(){
		glPushMatrix(); {
			glColor3f(c.r, c.g, c.b);
			glTranslatef(xCenter, yMin, zCenter);
			glRotatef(-90, 1, 0, 0);
			GLUquadricObj  *quadric = gluNewQuadric();
			gluCylinder(quadric,radius, radius,(yMax-yMin), 50, 50);
			//gluDisk(quadric, 0, radius, 50, 50);
			//glColor3f(1, 0, 0);
			glTranslatef(0, 0, yMax - yMin);
			//GLUquadricObj  *quadric = gluNewQuadric();
			gluDisk(quadric, 0, radius, 50, 50);
		}glPopMatrix();

		

	}
	void input(){
		char s[50];
		while (scanf("%s", s) == 1)
		{
			if (strcmp(s, "objEnd") == 0)break;
			else if (strcmp(s, "color") == 0)
			{
				c.input();
			}
			else if (strcmp(s, "ambCoeff") == 0){
				scanf("%lf", &ambientCoeff);
			}
			else if (strcmp(s, "difCoeff") == 0){
				scanf("%lf", &diffuseCoeff);
			}
			else if (strcmp(s, "refCoeff") == 0){
				scanf("%lf", &refCoeff);
			}
			else if (strcmp(s, "specCoeff") == 0){
				scanf("%lf", &specCoeff);
			}
			else if (strcmp(s, "specExp") == 0){
				scanf("%lf", &specExp);
			}
			else if (strcmp(s, "xCenter") == 0){
				scanf("%lf",&xCenter);
			}
			else if (strcmp(s, "zCenter") == 0){
				scanf("%lf", &zCenter);
			}
			else if (strcmp(s, "yMin") == 0){
				scanf("%lf", &yMin);
			}
			else if (strcmp(s, "yMax") == 0){
				scanf("%lf", &yMax);
			}
			else if (strcmp(s, "radius") == 0){
				scanf("%lf", &radius);
			}

		}
	}
	double rayIntersectsShape(Ray ray){
		double a, b, c,d,t;

		a = ray.direction.x*ray.direction.x + ray.direction.z*ray.direction.z;
		b = 2 * ((ray.origin.x - xCenter)*ray.direction.x + (ray.origin.z - zCenter)*ray.direction.z);
		c = (ray.origin.x - xCenter)*(ray.origin.x - xCenter) + (ray.origin.z - zCenter)*(ray.origin.z - zCenter) - radius*radius;

		d = b*b - 4 * a*c;
		t = 50000;
		
		if (d < 0)t = 50000;
		else if (d == 0)
		{
			t= -b / 2 * a;
			double y = ray.origin.y + t*ray.direction.y;
			if (y >= yMin && y <= yMax)t=50000;
		}
		else
		{
			double t1, t2,y1,y2;
			t1 = (-b + sqrt(d)) / 2 / a;
			t2 = (-b - sqrt(d)) / 2 / a;

			y1 = ray.origin.y + t1*ray.direction.y;
			y2 = ray.origin.y + t2*ray.direction.y;

			if (y1 >= yMin && y1 <= yMax && y2 >= yMin && y2 <= yMax)
			{
				if (t1 > t2)t=t2;
				else t=t1;
			}
			else if (y1 >= yMin && y1 <= yMax)
			{
				t = t1;
			}
			else if (y2 >= yMin && y2 <= yMax)
			{
				t = t2;
			}
		}

		double upperLidT = (yMax - ray.origin.y) / ray.direction.y;
		Vector3d upperLidIntersectPoint(ray.origin,upperLidT,ray.direction);
		
		if (upperLidIntersectPoint.distance(Vector3d(xCenter, yMax, zCenter)) <= radius && t>=upperLidT)
		{
			t = upperLidT;
		}

		double lowerLidT = (yMin- ray.origin.y) / ray.direction.y;
		Vector3d lowerLidIntersectPoint(ray.origin, lowerLidT, ray.direction);
		if (lowerLidIntersectPoint.distance(Vector3d(xCenter, yMin, zCenter)) <= radius && t >= lowerLidT)
		{
			t = lowerLidT;
		}
		if (t == 50000)return -1;
		return t;
	}
	Color getColorAtPoint(Ray ray, Vector3d point, Lights l, vector<Shape *> allShapes){
		
		Color color(0, 0, 0);
		int lightCount = l.allLights.size();
		if (point.y == yMax)
		{
			Vector3d normal(0, 1, 0);
			normal = normal.normalize();
			for (int i = 0; i < lightCount; i++)
			{
				Vector3d incidentRay(point, *(l.allLights[i]));
				incidentRay = incidentRay.normalize();
				//normal = normal.normalize();

				Ray inc(*(l.allLights[i]), incidentRay);
				double mint = 500000;
				Shape *intersectedObject = NULL;
				int shapeCount = allShapes.size();
				for (int j = 0; j<shapeCount; j++)
				{

					double t = (allShapes[j])->rayIntersectsShape(inc);

					if (t > 0 && t<mint)
					{
						intersectedObject = allShapes[j];
						mint = t;
						break;
					}
				}

				if (mint < 50000)continue;

				//Vector3d reflection = incidentRay - normal * 2 * (incidentRay.dot(normal));
				double lambert = incidentRay.dot(normal);

				Vector3d viewer(point, ray.origin);
				//Vector3d viewer(ray.origin, point);
				viewer = viewer.normalize();
				Vector3d halfVector = incidentRay + viewer;
				halfVector = halfVector.normalize();
				double phong = halfVector.dot(normal);


				Color diff = c*(diffuseCoeff*(lambert > 0 ? lambert : 0));
				double specPart = pow((phong > 0 ? phong : 0), specExp);
				Color spec = c*(specCoeff*specPart);

				printf("%lf %lf\n", lambert, phong);

				color = color + (diff + spec)*(1.0 / lightCount);
			}
		}
		else if (point.y == yMin)
		{
			Vector3d normal(0, -1, 0);
			normal = normal.normalize();
			for (int i = 0; i < lightCount; i++)
			{
				Vector3d incidentRay(point, *(l.allLights[i]));
				incidentRay = incidentRay.normalize();
				//normal = normal.normalize();

				//Vector3d reflection = incidentRay - normal * 2 * (incidentRay.dot(normal));
				double lambert = incidentRay.dot(normal);

				Vector3d viewer(point, ray.origin);
				//Vector3d viewer(ray.origin, point);
				viewer = viewer.normalize();
				Vector3d halfVector = incidentRay + viewer;
				halfVector = halfVector.normalize();
				double phong = halfVector.dot(normal);


				Color diff = c*(diffuseCoeff*(lambert > 0 ? lambert : 0));
				double specPart = pow((phong > 0 ? phong : 0), specExp);
				Color spec = c*(specCoeff*specPart);

				printf("%lf %lf\n", lambert, phong);

				color = color + (diff + spec)*(1.0 / lightCount);
			}
		}
		else
		{
			Vector3d center(xCenter, point.y, zCenter);
			Vector3d normal(center, point);
			normal = normal.normalize();
			for (int i = 0; i < lightCount; i++)
			{
				Vector3d incidentRay(point, *(l.allLights[i]));
				incidentRay = incidentRay.normalize();
				//normal = normal.normalize();

				//Vector3d reflection = incidentRay - normal * 2 * (incidentRay.dot(normal));
				double lambert = incidentRay.dot(normal);

				Vector3d viewer(point, ray.origin);
				//Vector3d viewer(ray.origin, point);
				viewer = viewer.normalize();
				Vector3d halfVector = incidentRay + viewer;
				halfVector = halfVector.normalize();
				double phong = halfVector.dot(normal);


				Color diff = c*(diffuseCoeff*(lambert > 0 ? lambert : 0));
				double specPart = pow((phong > 0 ? phong : 0), specExp);
				Color spec = c*(specCoeff*specPart);

				//printf("%lf %lf\n", lambert, phong);

				color = color + (diff + spec)*(1.0 / lightCount);
			}

		}

		color = color + c* ambientCoeff;
		
		return color;
	}
	
	Vector3d getNormal(Vector3d point, Ray ray){
		if (point.y == yMax)
		{
			Vector3d normal(0, 1, 0);
			normal = normal.normalize();

			return normal;
		}
		else if (point.y == yMin)
		{
			Vector3d normal(0, -1, 0);
			normal = normal.normalize();
			return normal;
		}
		else
		{
			Vector3d center(xCenter, point.y, zCenter);
			Vector3d normal(center, point);
			normal = normal.normalize();
			return normal;
		}
	}

};
#endif